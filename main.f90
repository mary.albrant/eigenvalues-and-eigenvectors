program eigenvalues_and_eigenvectors_for_simetric_matrix
use method_Jacobi 
implicit none
  
  integer :: n, i, j
  real(8) :: eps
  real(8), allocatable, dimension(:,:) :: A, eigen_valvec
  
  open(1, file = "input");  open(2, file = "output")
  read(1,*) n
  allocate(A(n,n),eigen_valvec(n,n+1))
  do i = 1, n
     read(1,*) A(i,:)
  enddo
  read(1,*) eps
  
  eigen_valvec = Jacobi(n,A,eps)
  write(2,*) "eigen_val :", eigen_valvec(:,1)
  write(2,*) " "

  do i = 1, n
     write(2,*) "eigen_vec ", i, ":", (eigen_valvec(i,j), j = 2,n+1)
  enddo
    
end program
