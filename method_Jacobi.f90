module method_Jacobi
contains
  
function Jacobi(n,A,eps) result(res)
  implicit none
  
  integer :: n, i, j, flag
  integer, dimension(2) :: indx, indx1
  real(8) :: eps, phi
  real(8), dimension(n,n) :: A, upprA, downA, U
  real(8), dimension(n,n+1) :: res
  
  do i = 1, n
     upprA(i,:) = A(i,i+1:n)
     downA(i,:) = A(i,1:i-1)
  enddo 
  
  do while ( sqrt(sum((upprA + downA)**2)) > eps )
     indx1 = maxloc(abs(upprA(:,:)))
     indx = maxloc(abs(downA(:,:)))
  
     if (A(indx1(1),indx1(2)) > A(indx(1),indx(2))) then
        indx = indx1
     endif
     
     if (A(indx(1),indx(1)) == A(indx(2),indx(2))) then
        phi = atan(1.0)
     else    
        phi = 0.5*atan(2.0*A(indx(1),indx(2))/(A(indx(1),indx(1)) - A(indx(2),indx(2))))
     endif
     
     do i = 1, n
        U(i,i) = 1.0 
     enddo     
     U(indx(1),:) = A(indx(1),:)
     U(indx(2),:) = A(indx(2),:)
     U(:,indx(1)) = A(:,indx(1))
     U(:,indx(2)) = A(:,indx(2))
     U(indx(1),indx(1)) = cos(phi)
     U(indx(1),indx(2)) = -sin(phi)
     U(indx(2),indx(1)) = sin(phi)
     U(indx(2),indx(2)) = cos(phi)

     A = matmul(matmul(transpose(U),A),U)
     
     do i = 1, n
        upprA(i,:) = A(i,i+1:n)
        downA(i,:) = A(i,1:i-1)
     enddo     
  enddo
 
  do i = 1, n
     res(i,1) = A(i,i)
     res(i,2:n+1) = U(:,i)
     j = 1; flag = 0
     do while (flag == 0)
     j = j + 1
        if (abs(res(i,j)) > eps**2) then
           res(i,2:n+1) = res(i,2:n+1)/res(i,j)
           flag = 1
        endif
     enddo 
  enddo
    
end function Jacobi  

end module method_Jacobi
